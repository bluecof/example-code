package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.exception.RobotCoreException;
import com.qualcomm.robotcore.hardware.Gamepad;

/**
 * Created by Ben on 4/22/2016.
 * Base OpMode class for all 7468 teleop OpModes
 * Automatically sets standard servo positions to saved states at init
 * and saves the gamepad states from the previous loop cycle.
 */
public abstract class OpMode7468Tele extends OpMode7468 {
    @Override
    public void init() {
        super.init();

        // Set standard servos to end of autonomous positions
        for (int i = 0; i < standardServos.length; i++) {
            standardServos[i].setPosition(servoPositions[i]);
        }
    }
}
