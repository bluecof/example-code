package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.ftccommon.DbgLog;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.exception.RobotCoreException;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Ben on 4/22/2016.
 * Base OpMode class for all 7468 OpModes, both autonomous and teleop
 * Automatically pulls out all hardware devices from the hardware map
 * and puts them into variables at init. Also saves the positions of
 * standard servos in static variables
 */
public abstract class OpMode7468 extends OpMode {
    // Hardware devices
    protected DcMotor intake;
    protected DcMotor driveLeft;
    protected DcMotor driveRight;
    protected DcMotor elbow;
    protected DcMotor wrist;
    protected DcMotor winch;
    protected Servo shoulder;
    protected Servo superArm;
    protected Servo antenna;
    protected GyroSensor gyro;

    // List of standard servos
    protected Servo[] standardServos;
    // Standard servo positions to save between opmodes
    protected static double[] servoPositions = null;

    // Old gamepad states
    protected Gamepad gamepad1Old;
    protected Gamepad gamepad2Old;

    @Override
    public void init() {
        // Pull out hardware configuration and save into variables
        intake = hardwareMap.dcMotor.get("intake");
        driveLeft = hardwareMap.dcMotor.get("driveLeft");
        driveRight = hardwareMap.dcMotor.get("driveRight");
        elbow = hardwareMap.dcMotor.get("elbow");
        wrist = hardwareMap.dcMotor.get("wrist");
        winch = hardwareMap.dcMotor.get("winch");
        shoulder = hardwareMap.servo.get("shoulder");
        superArm = hardwareMap.servo.get("superArm");
        antenna = hardwareMap.servo.get("antenna");
        gyro = hardwareMap.gyroSensor.get("gyro");

        // Setup standard servo position saving here, if not already setup
        standardServos = new Servo[]{superArm};
        if (servoPositions != null) {
            servoPositions = new double[]{0.0d};
        }

        // Do device setup here (e.g. reversing motors/servos, calibrating sensors)
        gyro.calibrate();

        // Save initial gamepad states
        copyGampepads();
    }

    @Override
    public void init_loop() {
        // Save gamepad states
        copyGampepads();
    }

    @Override
    public void loop() {
        // Save old gamepad states
        copyGampepads();
    }

    // Copies gamepad states to store old states
    private void copyGampepads() {
        // Save old gamepad states
        try {
            gamepad1Old.copy(gamepad1);
        } catch (RobotCoreException e) {
            // Use default state;
            gamepad1Old = new Gamepad();
            DbgLog.error("Error copying gamepad1");
        }
        try {
            gamepad2Old.copy(gamepad2);
        } catch (RobotCoreException e) {
            // Use default state;
            gamepad2Old = new Gamepad();
            DbgLog.error("Error copying gamepad2");
        }
    }
}
