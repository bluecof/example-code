package com.qualcomm.ftcrobotcontroller.opmodes;

/**
 * Created by Ben on 4/22/2016.
 * Base OpMode class for all 7468 autonomous OpModes
 * Provides no extra functionality, yet
 */
public abstract class OpMode7468Auto extends OpMode7468 {
    @Override
    public void loop() {
        super.loop();

        // Save servo positions
        for (int i = 0; i < standardServos.length; i++) {
            servoPositions[i] = standardServos[i].getPosition();
        }
    }
}
